$(function(){
	$('#noSkill').click(function()
	{
		if($(this).prop('checked')){
			$('input:not(#noSkill)').prop('checked',false);
		}
	});
	
	$('input:not(#noSkill):not(input[type=submit])').click(function()
	{
		$('#noSkill').prop('checked',false);
	});
});