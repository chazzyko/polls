<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	$sess_token = Session::token();
	
	if(!isset($sess_token) || trim($sess_token) == '') $sess_token = 'no Token';
	
	$form_token = Input::get('_token');
	
	Log::info('TOKEN', ['<<<-- GOOD Sess TOKEN --->>>'=> $sess_token, '<<<-- GOOD input token -->>>'=> $form_token]);
	
	if ($sess_token != $form_token)
	{	
		Log::info('<<EE----- ERROR ---->>', ['<<<E-- ERR Sess TOKEN --->>>'=> $sess_token, '<<<EE-- ERR input token -->>>'=> $form_token]);
		
		App::error(function(Exception $exception, $sess_token, $form_token)
		{				
				Log::error($exception, ['<<< ERROR Session Token >>>' => $sess_token, '<<< ERROR Form Token >>>'=>$form_token]);
		});
		
		throw new Illuminate\Session\TokenMismatchException;
	}
});
