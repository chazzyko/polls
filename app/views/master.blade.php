<!DOCTYPE html>
<html lang="lt">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>{{ $title }}</title>	
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
 		{{ HTML::script('js/jquery211.min.js') }}
 		{{ HTML::script('js/bootstrap.min.js') }}
 	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"> 
</head>
<body>
	<div class="row">	
		<div class="col-md-8 pull-right">
			<h1>{{ $head_tag }}</h1>
			@yield('content')
		</div>
	</div>
	@yield('footer_extras')
</body>
</html>
