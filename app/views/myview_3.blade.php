@extends('master')

@section('content')
<div class="row">

	<h3>Lytis</h3>
	{{ Form::open(['route'=>['users.store', 'id'=>$id], 'id'=>'form3', 'class'=>'form-horizontal']) }}
	
			<div class="radio-inline">
				{{ Form::label("Vyras") }}
				{{ Form::radio('sex', 'Vyr') }}
			</div>	
			<div class="radio-inline">
				{{ Form::label("Moteris") }}
				{{ Form::radio('sex', 'Mot') }}
			</div>		
	
		@if($errors->has('sex'))<p class="help-block alert alert-danger">{{ $errors->first('sex') }}</p>@endif
		
		{{ Form::submit(trans('user.send'),['class'=>'btn btn-default']) }}
	{{ Form::close() }}
</div>
	<script>
	function submitForm()
	{
		document.getElementById("form3").submit();
	}
		//window.onload = submitForm;
		
	</script>	
@stop