@extends('master')

@section('content')
<div class="row">
	<h3>Kokias prgramavimo kalbas mokate ?</h3>
	{{ Form::open(['route'=>['users.store', 'id'=>$id], 'id'=>'form_prog_langs']) }}
		<div class="checkbox">
			{{ Form::label("PHP") }}
			{{ Form::checkbox('code_lang_1', 'PHP') }}
		</div>
		<div class="checkbox">
			{{ Form::label("CSS") }}
			{{ Form::checkbox('code_lang_2', 'CSS') }}
		</div>
		<div class="checkbox">
			{{ Form::label("HTML") }}
			{{ Form::checkbox('code_lang_3', 'HTML') }}
		</div>
		<div class="checkbox">
			{{ Form::label("javaScript") }}
			{{ Form::checkbox('code_lang_4', 'javaScript') }}
		</div>
		<div class="checkbox">
			{{ Form::label("Java") }}
			{{ Form::checkbox('code_lang_5', 'Java') }}
		</div>
		<div class="checkbox">
			{{ Form::label("Nemoku nė vienos") }}
			{{ Form::checkbox('code_lang_6', 'Nemoku', null, ['id'=>'noSkill']) }}
		</div>
		@if($errors->any())<p class="help-block alert alert-danger">{{ $errors->first() }}</p>@endif
		
		{{ Form::submit(trans('user.send'),['class'=>'btn btn-default']) }}
		
	{{ Form::close() }}
</div>
@stop
@section('footer_extras')
	{{ HTML::script('js/scripts.js')}}
@stop