@extends('master')
@section('content')
<div class="row">
	{{ Form::open(['route'=>['users.store', 'id'=>$id], 'id'=>'form2']) }}	
		{{ Form::label("Vardas") }}		
			{{ Form::text('name') }}
			@if($errors->has('name')) <p class="help-block alert alert-danger">{{ $errors->first('name') }}</p>@endif
		{{ Form::submit(trans('user.send'),['class'=>'btn btn-default']) }}
	{{ Form::close() }}
</div>
@stop