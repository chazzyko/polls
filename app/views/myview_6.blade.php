@extends('master')

@section('content')
<div class="row">
	<h3>Jūsų foto</h3>
	{{ Form::open(['route'=>['users.store', 'id'=>$id], 'class'=>'form-file',  'files'=>true]) }}
		<div class="btn btn-default btn-file">
			{{ Form::label("Jūsų foto") }}
			{{ Form::file('photo') }}
		</div>
		@if($errors->has('photo')) <p class="help-block alert alert-danger">{{ $errors->first('photo') }}</p>@endif
		
		{{ Form::submit(trans('user.send'),['class'=>'btn btn-default']) }}
		
	{{ Form::close() }}
</div>
@stop