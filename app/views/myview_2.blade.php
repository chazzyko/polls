@extends('master')
@section('content')	
	{{ Form::open(['route'=>['users.store', 'id'=>$id], 'id'=>'form2']) }}
		{{ Form::label("Gimimo data") }}
		{{ Form::text('dob',null,['placeholder'=>'Metai-mėnuo-diena']) }}
		@if($errors->has('dob')) <p class="help-block alert alert-danger">{{ $errors->first('dob') }}</p>@endif
		{{ Form::submit(trans('user.send'),['class'=>'btn btn-default']) }}
	{{ Form::close() }}	
@stop

