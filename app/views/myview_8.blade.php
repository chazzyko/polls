@extends('master')

@section('content')
<div class="row">
	<div class="col-md-7">
		<h3>Jūsų anketos atsakymai:</h3>
		@if(isset($polls) && count($polls) > 0 )
		<div class="table-responsive">
			<table class="table">
				@foreach ($polls as $key => $poll)
				<tr>
					<td>Vardas</td><td>{{ $poll->name }}</td>
				</tr>
				<tr>
					<td>Gim. data</td><td>{{ $poll->dateofbirth }}</td>
				</tr>
				<tr>
					<td>Lytis</td><td>{{ $poll->sex }}</td>
				</tr>
				<tr>
					<td>Ar domitės programavimu</td><td>{{ $poll->coding_interest }}</td>
				</tr>
				<tr>
					<td>Kokias programavimo kalbas mokate</td><td>{{ $poll->coding_langs }}</td>
				</tr>
				@if(trim($poll->photo_link) !== '')
					<tr>
						<td>Jūsų foto</td><td><img src="{{ url() . '/' . $poll->photo_link }}" alt="Foto" /></td>
					</tr>
				@endif
				@endforeach
			</table>
		</div>
		@else
			<p>Anketos duomenų nerasta.</p>
		@endif
	</div>
</div>
@stop