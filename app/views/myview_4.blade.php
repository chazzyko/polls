@extends('master')

@section('content')
<div class="row">
	<h3>Ar domitės programavimu ?</h3>
	{{ Form::open(['route'=>['users.store', 'id'=>$id], 'id'=>'form4']) }}
		<div class="radio-inline">
			{{ Form::label("Taip") }}
			{{ Form::radio('coding', 'Taip') }}
		</div>
		<div class="radio-inline">
			{{ Form::label("Ne") }}
			{{ Form::radio('coding', 'Ne') }}
		</div>
		@if($errors->has('coding')) <p class="help-block alert alert-danger">{{ $errors->first('coding') }}</p>@endif
		{{ Form::submit(trans('user.send'),['class'=>'btn btn-default']) }}
	{{ Form::close() }}	
</div>
@stop