<?php
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
		$aCookie = Cookie::get('poll_cookie');
		
		$id = (isset($aCookie["idd"])) ? (int)$aCookie["idd"] : 1;
		$aCookie["idd"] = $id;
						
		$this->setCookie($aCookie);
		
		if(isset($aCookie) && isset($aCookie["finished"])){
			$id = $aCookie["finished"];
		}
		
		return Redirect::route('users.show', [$id]);		
    }

	
	protected function setCookie($arr){
		Cookie::queue(Cookie::make('poll_cookie', $arr, 43200));
	}
	
	protected function storeUser($input_arr)
	{
		$user = new User();
		$user->name = $input_arr["bio"]["name"];
		$user->dateofbirth = date('Y-m-d', strtotime($input_arr["bio"]["dob"]));
		$user->sex = $input_arr["bio"]["sex"];
		$user->photo_link = (isset($input_arr["photo"])) ? $input_arr["photo"] : '';
		$user->save();
		
		return $user->id;
	}
	

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {		
		$input = Request::all();
		
		$input_arr = Cookie::get('poll_cookie');
		
		if(isset($input_arr["finished"])) return Redirect::route('users.show', [$input_arr["finished"]]);
		
				
		if($id < 5){
			$validator = Validator::make($input, User::${'rules_'.$id});
			
			if( $validator->fails() ){
				$errors = $validator->messages();
				return Redirect::back()->withInput()->withErrors($validator);
			}
			
		}elseif($id == 5){
			$count = 0;
			$errors = '';
			foreach($input as $inputKey => $inputValue)
			{
				if($inputKey != '_token' && trim($inputValue) != '') {
					$count++;
					$input_name['code_lang'] = $inputValue;
					$validator = Validator::make($input_name, User::${'rules_'.$id});
					if($validator->fails()){
						$errors = $validator->messages()->toArray();
						$errors = $errors["code_lang"][0];
						break;
					}
				}
			}
			
			if($count < 1) $errors = 'Privaloma pažymėti bent vieną laukelį.';
			if($count > 5) $errors = 'Klaida.';
			
			if($errors) return Redirect::back()->withInput()->withErrors($errors);
			
			
		}elseif($id == 6 && Input::hasFile('photo')){
			
			if(Input::file('photo')->isValid()){
								
				$file = Input::file('photo');
				$file_name = time(). '-'. $file->getClientOriginalName();
				$aFile = ['photo' => $file ];
				
				$validator = Validator::make($aFile, User::${'rules_'.$id});
				
				if($validator->fails()) return Redirect::back()->withErrors($validator);
				
				$input_arr["photo"] = 'uploads/img/'. $file_name;				
				
				$file->move("uploads/img/", $file_name);
			}
			
		}
		
		
		
		if($id > 3 && $id < 6){
			$tmp = (isset($input_arr["poll"]["coding"])) ? $input_arr["poll"]["coding"] : '';			
			unset($input_arr["poll"]);
			$input_arr["poll"]["coding"] = (isset($input["coding"])) ? $input["coding"] : $tmp;
		}
		
		foreach($input as $k=>$v)
		{
			if($k !== '_token' && $k !== 'photo'){
				if($id < 4)
					$input_arr["bio"][$k] = $v;
				else
					$input_arr["poll"][$k] = $v;
			}
		}
		
		$id++;

		
		$input_arr["idd"] = $id;
		
		
		if(!isset($input_arr['finished'])) $this->setCookie($input_arr);
		
		if( ($id == 5 && strtolower($input['coding']) == 'ne') || ($id == 6 && isset($input["code_lang_6"]) ) ){
			
			$user_id = $this->storeUser($input_arr);
									
			$poll = new Poll();
			$poll->user_id = $user_id;
			$poll->coding_interest = $input_arr["poll"]["coding"];
			$poll->coding_langs = ($id == 6) ? 'Nemoku' : '';
			
			$poll->save();
			
			$page_num = 7;
			$input_arr['finished'] = $page_num;
			$input_arr["idd"] = $page_num;
			
			$this->setCookie($input_arr);
			
			return Redirect::route('users.show', [$page_num]);
		}
		
		if($id == 7){
			
			$user_id = $this->storeUser($input_arr);
			
			$poll = new Poll();
			$poll->user_id = $user_id;
			$poll->coding_interest = $input_arr["poll"]["coding"];
			
			$coding_langs = '';
			foreach($input_arr["poll"] as $key=>$val)
			{
				if($key != 'coding'){
					$coding_langs .= $val . ', ';
				}
			}
			
			$coding_langs = rtrim($coding_langs, ', ');
			
			$poll->coding_langs = $coding_langs;
			
			$poll->save();
			
			$page_num = 8;
			$input_arr['finished'] = $page_num;
			$input_arr["idd"] = $page_num;
			$input_arr["user_id"] = $user_id;
			$this->setCookie($input_arr);
			return Redirect::route('users.show', [$page_num]);
		}
				
		return Redirect::route('users.show', [$id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$cookie = Cookie::get('poll_cookie') ? Cookie::get('poll_cookie') : [];
		if(!isset($cookie["idd"])) $cookie["idd"] = 1;
		
		if ($cookie["idd"] - $id !== 0) return Redirect::route('users.show',[$cookie["idd"]]);
							
		if (isset($cookie) && isset($cookie["finished"])) $id = $cookie["finished"];
		
		$polls = [];
		if (isset($cookie["user_id"]) && trim($cookie["user_id"]) !== ''){

			$uid = (int)$cookie["user_id"];
									
			$polls = DB::table('users')
				->join('polls', function($join) use($uid)
				{
					$join->on('users.id', '=','polls.user_id')
						->where('users.id', '=', $uid);
				})
					->get();
		}
		
        return View::make('myview_'.$id)
			->with('title', 'Anketa')
			->with('head_tag', 'Apklausos anketa')			
			->with('id', $id)
			->with('polls', $polls);
    }

   
}