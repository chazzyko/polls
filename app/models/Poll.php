<?php
class Poll extends Eloquent
{	
    protected $fillable = ['coding_interest', 'coding_langs'];

	public function users()
	{
		$this->belongsTo('User');
	}	
}