<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 	 
	public static $rules_1 = ['name' => 'required|alpha_num'];
	public static $rules_2 = ['dob' => 'required|date_format:Y-m-d'];
	public static $rules_3 = ['sex' => 'required|alpha'];
	public static $rules_4 = ['coding' => 'required|alpha'];
	public static $rules_5 = ['code_lang' => 'alpha'];
	public static $rules_6 = ['photo' => 'mimes:jpeg,bmp,png|image|max:500'];
	 	 
	protected $fillable = ['name','dateofbirth', 'sex', 'photo_link']; 
	
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
	
	
	public function getRememberToken(){
		return $this->remember_token;
	}

	public function setRememberToken($value){
		return $this->remember_token = $value;
	}

	public function getRememberTokenName(){
		return "rememberToken";
	}
	
	public function polls()
	{
		return $this->hasMany("Poll");
	}

}
