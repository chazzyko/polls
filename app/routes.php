<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',[		
	'as' => 'users.index',
	'uses' => 'UserController@index'
]);

Route::get('anketa', function()
{	
	return Redirect::route('users.show', [1]);
});

Route::get('anketa/{id}',[		
	'as' => 'users.show',
	'uses' => 'UserController@show'
])->where('id','[0-9]+');

Route::group(['before'=>'csrf'], function()
{
	Route::post('form/{id}',[		
		'as' => 'users.store',
		'uses' => 'UserController@store'
	]);

	Route::post('poll/{id}',[		
		'as' => 'polls.store',
		'uses' => 'PollController@store'
	]);
});


App::error(function($exception)
{
	Log::warning('MethodNotAllowedHttpException', ['context'=> $exception->getMessage()]);
	
	return Response::view('errors.nopage', ['title'=>'Klaida', 'head_tag'=>'Apklausos anketa'], 404);
	
});

App::missing(function($exception)
{
    return Response::view('errors.nopage', ['title'=>'Klaida', 'head_tag'=>'Apklausos anketa'], 404);
});
